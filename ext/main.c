#include "stat_analysis.h"
#include <stdio.h>
#include <stdlib.h>

// program name, database, output filename
#define MIN_ARGS 2

#define STR_BUFFER_SIZE (256)

int main(int argc, char *argv[]) {
  char *annotator = "atest";
  char *reference_annotator = "atr";
  char *record_filename = "RECORDS.txt";
  char error_filename[STR_BUFFER_SIZE];

  error_filename[0] = '\0';
  my_strlcat(error_filename, "Rhythm_Errors_", STR_BUFFER_SIZE);

  int8_t res = 0;

  if (argc < MIN_ARGS) {
    fprintf(
        stderr,
        "Usage: stat_analysis [output_file] [annotator (opt, default atest)]\n"
        "[reference_annotator (opt, default atr)]\n"
        "[record_filename (opt, default RECORDS.txt)]\n"
        "[error_filename (opt, default Rhythm_Errors_[annotator])]\n");
    exit(-1);
  }

  if (argc > MIN_ARGS) {
    fprintf(stderr, "Ann(%s)\n", argv[2]);
    annotator = argv[2];
  }

  // We have the correct annotator for the end now
  my_strlcat(error_filename, annotator, STR_BUFFER_SIZE);
  my_strlcat(error_filename, ".txt", STR_BUFFER_SIZE);

  if (argc > MIN_ARGS + 1) {
    fprintf(stderr, "Ref(%s)\n", argv[3]);
    reference_annotator = argv[3];
  }

  if (argc > MIN_ARGS + 2) {
    fprintf(stderr, "Record_file(%s)\n", argv[4]);
    record_filename = argv[4];
  }

  if (argc > MIN_ARGS + 3) {
    uint16_t len = strlen(argv[5]);
    if (len > STR_BUFFER_SIZE) {
      return 1;
    }
    fprintf(stderr, "Error_file(%s)\n", argv[4]);
    strcpy(error_filename, argv[5]);
  }

  return stat_analysis(argv[1], annotator, reference_annotator, record_filename,
                       error_filename);
}
