#ifndef STAT_ANALYSIS_H
#define STAT_ANALYSIS_H

#include <stdint.h>

#include <stdio.h>
#include <string.h>

#include "wfdb/ecgmap.h"
#include "wfdb/wfdb.h"

typedef enum {
  SUCCESS,
  IO_ERROR,
  ANNOTATION_FILE_EMPTY,
  ANNOTATION_FILE_BAD_TIME,
  NUM_ERRORS
} stat_errors;

int32_t stat_analysis(const char *ofname, const char *annotator,
                      const char *reference_annotator,
                      const char *record_filename, const char *error_filename);
size_t my_strlcat(char *dst, const char *src, size_t siz);

// Global vars

extern int32_t match_dt; /* match window duration in samples */
extern long start;       /* time of the beginning of the test period */
extern long end_time;    /* end of the test period (-1: end of reference annot
                            file; 0: end of either annot file) */
extern FILE *ofile, *sfile;   /* files for beat-by-beat and shutdown reports */
extern char *ofname, *sfname; /* filenames for reports */
extern WFDB_Anninfo an[2];
extern long shut_down;  /* duration of test annotator's shutdown */
extern int32_t verbose; /* if non-zero, prints out additional info to the screen
                         when there's a mismatch between the physician
                         annotation (ref) and the algorithm-generated annotation
                         (test) */
extern int32_t
    record_info; /* If this is set to one, print record by record information
                (confusion matrices, etc) to the output file. If it is set to
                zero, only print a summary matrix. */
extern uint32_t oflag; /* if non-zero, produce an output annotation file */
extern struct WFDB_ann ref_annot;
extern struct WFDB_ann test_annot;

extern int32_t A,
    Aprime; /* types of the current & next reference annotations */
extern int32_t a, aprime; /* types of the current & next test annotations */
extern long huge_time;    /* largest possible time */
extern long T, Tprime; /* times of the current & next reference annotations */
extern long t, tprime; /* times of the current & next test annotations */

// This holds the statistical output of the beat by beat comparison.
typedef struct {
  int64_t QTP, QFN, QFP, STP, SFN, STN, SFP, VTP, VFN, VTN, VFP;
  int64_t Nn, Sn, Vn, Fn, On, Ns, Ss, Vs, Fs, Os, Nv, Sv, Vv, Fv, Ov, No, So,
      Vo, Fo;
} bxb_stats_t;

// This holds the statistical output of the run by run comparison.
typedef struct {
  int64_t CTPs, CFN, CTPp, CFP, STPs, SFN, STPp, SFP, LTPs, LFN, LTPp, LFP;
} rxr_stats_t;

#endif // STAT_ANALYSIS_H
