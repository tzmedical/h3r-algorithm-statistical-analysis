#include "stat_analysis.h"
#include "errno.h"

#define INITIAL_BUFFER_SIZE (1024)
#define STR_BUFFER_SIZE (256)

// How much data at the beginning of the record should be disregarded
// as setup/training time for the algorithm. Default is 5 minutes, but
// some databases only have 5 minute long records, so a smaller time
// must be used.
// Default "5:0"
#define LEARNING_TIME ("2:0")

// Called functions
bxb_stats_t *bxb(FILE *ofile, char *record);
int32_t rxr(FILE *ofile, char *record);
int32_t vf_af(int32_t argc, char *argv[]);

// Local functions
void quotient_pstat(FILE *ofile, char *f, long a, long b);

// Global variables
int32_t match_dt = 0; /* match window duration in samples */
long start;           /* time of the beginning of the test period */
long end_time; /* end of the test period (-1: end of reference annot file; 0:
                  end of either annot file) */
FILE *ofile, *sfile; /* files for beat-by-beat and shutdown reports */
WFDB_Anninfo an[2];
char record[STR_BUFFER_SIZE]; /* record name */
long shut_down;               /* duration of test annotator's shutdown */
int32_t verbose =
    0; /* if 0, no output.
             if 1, prints out information about the records ones they are
          processed. if 2, prints out additional info to the screen. H when
          there's a mismatch between the physician annotation (ref) and the
          algorithm-generated annotation (test)
          */

int32_t record_info =
    1; /* If this is set to one, print record by record information (confusion
         matrices, etc) to the output file. If it is set to zero, only print a
         summary matrix. */

int32_t A, Aprime; /* types of the current & next reference annotations */
int32_t a, aprime; /* types of the current & next test annotations */
long huge_time = 0x7FFFFFFF; /* largest possible time */
long T, Tprime;     /* times of the current & next reference annotations */
long t, tprime;     /* times of the current & next test annotations */
uint32_t oflag = 0; /* if non-zero, produce an output annotation file */
struct WFDB_ann ref_annot;
struct WFDB_ann test_annot;

char *my_strdup(const char *s) {
  size_t len = strlen(s) + 1;
  void *new = malloc(len);

  if (new == NULL)
    return NULL;

  return (char *)memcpy(new, s, len);
}

void print_total_stats(FILE *ofile, bxb_stats_t *stats) {
  if (record_info) {
    (void)fprintf(
        ofile,
        "______________________________________________________________________"
        "______________________________________________________________________"
        "________________________________________________________\n");
    (void)fprintf(ofile,
                  "%-10s   %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld "
                  "%6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld\n",
                  " Sum", stats->Nn, stats->Sn, stats->Vn, stats->Fn, stats->On,
                  stats->Ns, stats->Ss, stats->Vs, stats->Fs, stats->Os,
                  stats->Nv, stats->Sv, stats->Vv, stats->Fv, stats->Ov,
                  stats->No, stats->So, stats->Vo, stats->Fo);

    (void)fprintf(ofile, "Gross                                                "
                         "                                                     "
                         "                                        ");
    quotient_pstat(ofile, " %6.2f", stats->QTP,
                   stats->QTP + stats->QFN); // QRS sensitivity
    quotient_pstat(ofile, " %6.2f", stats->QTP,
                   stats->QTP + stats->QFP); // QRS positive predictivity
    quotient_pstat(ofile, " %6.2f", stats->VTP,
                   stats->VTP + stats->VFN); // VEB sensitivity
    quotient_pstat(ofile, " %6.2f", stats->VTP,
                   stats->VTP + stats->VFP); // VEB positive predictivity
    quotient_pstat(ofile, " %6.3f", stats->VFP,
                   stats->VTN + stats->VFP); // VEB false positive rate
    quotient_pstat(ofile, " %6.2f", stats->STP,
                   stats->STP + stats->SFN); // SVEB sensitivity
    quotient_pstat(ofile, " %6.2f", stats->STP,
                   stats->STP + stats->SFP); // SVEB positive predictivity
    (void)fprintf(ofile, "\n");
  }

  // These are the Gross stats reprinted
  fprintf(ofile,
          "\n         SE     Sp     +P    FPR   N_TP   N_FN   N_FP   N_TN\n");
  fprintf(ofile, "QRS ");
  quotient_pstat(ofile, " %6.2f", stats->QTP,
                 stats->QTP + stats->QFN); // QRS sensitivity
  quotient_pstat(ofile, " %6.2f", 0, 0);   // QRS specificity requires TN
  quotient_pstat(ofile, " %6.2f", stats->QTP,
                 stats->QTP + stats->QFP); // QRS positive predictivity
  quotient_pstat(ofile, " %6.2f", 0, 0); // QRS False Positive Rate requires TN
  fprintf(ofile, " %6d %6d %6d     - \n", stats->QTP, stats->QFN, stats->QFP);

  fprintf(ofile, "VEB ");
  quotient_pstat(ofile, " %6.2f", stats->VTP,
                 stats->VTP + stats->VFN); // VEB sensitivity
  quotient_pstat(ofile, " %6.2f", stats->VTN,
                 stats->VTN + stats->VFP); // VEB specificity
  quotient_pstat(ofile, " %6.2f", stats->VTP,
                 stats->VTP + stats->VFP); // VEB positive predictivity
  quotient_pstat(ofile, " %6.3f", stats->VFP,
                 stats->VTN + stats->VFP); // VEB false positive rate
  fprintf(ofile, " %6d %6d %6d %6d\n", stats->VTP, stats->VFN, stats->VFP,
          stats->VTN);

  fprintf(ofile, "SVEB");
  quotient_pstat(ofile, " %6.2f", stats->STP,
                 stats->STP + stats->SFN); // SVEB sensitivity
  quotient_pstat(ofile, " %6.2f", stats->STN,
                 stats->STN + stats->SFP); // SVEB specificity
  quotient_pstat(ofile, " %6.2f", stats->STP,
                 stats->STP + stats->SFP); // SVEB positive predictivity
  quotient_pstat(ofile, " %6.3f", stats->SFP,
                 stats->STN + stats->SFP); // SVEB false positive rate
  fprintf(ofile, " %6d %6d %6d %6d\n", stats->STP, stats->SFN, stats->SFP,
          stats->STN);
}

/** 'quotient_pstat' prints a statistic that is the quotient of a and b
 * expressed in percentage units. No-data values are indicated by '-'.
 */
void quotient_pstat(FILE *ofile, char *f, long a, long b) {
  // if there is no data for this particular statistic, then just print a dash
  if (b <= 0) {
    (void)fprintf(ofile, "     - ");
  }
  // otherwise, calculate statistic and use the given format for printing to
  // file
  else {
    (void)fprintf(ofile, f, (100. * a) / b);
  }
}

/**
 * Fetches a list of the records in a given database in wfdb format.
 * @param database The name of the database to read.
 * @param records A pointer to an array of strings. This will get
 * overwritten with the list of records.
 * @param size This will get overwritten with the number of records.
 * @return 0 for success, something else if an error occurs.
 */
int8_t get_record_list(const uint8_t *record_filename, char ***records,
                       uint32_t *size) {
  char line_buffer[STR_BUFFER_SIZE];

  char *record_name;
  char **temp = NULL;
  uint32_t current_buffer_size = INITIAL_BUFFER_SIZE;
  uint32_t i = 0;
  uint32_t j = 0;
  int8_t res = 0;
  FILE *record_file = calloc(1, sizeof(FILE));

  (*records) = malloc(current_buffer_size * sizeof(uint8_t *));

  record_file = fopen(record_filename, "r");
  if (NULL == record_file) {
    fprintf(stderr, "Unable to open (%s).\n", record_filename);

    return 1;
  }

  // Read the records from the file into an array.
  while (fgets(line_buffer, STR_BUFFER_SIZE, record_file) != NULL) {

    line_buffer[strcspn(line_buffer, "\n\r")] = '\0';
    if (line_buffer[0] != '\0' && line_buffer[0] != '#') {
      (*records)[i++] = my_strdup(line_buffer);

      // If we've reached the length of our buffer,
      // copy everything to one that's twice as large.
      if (i >= current_buffer_size) {
        temp = malloc(current_buffer_size * 2);
        for (j = 0; j < i; ++j) {
          temp[j] = (*records)[j];
        }
        free((*records));
        (*records) = temp;
        current_buffer_size *= 2;
      }
    }
  }

  *size = i;

  res = fclose(record_file);
  if (0 != res) {
    return 1;
  }
  return 0;
}

/**
 * Fetches a list of the valid records that encountered an error while being
 * analyzed.
 * @param records An array of records we care about errors from.
 * @param num The number of records in the array.
 * @param error_filename The name of the error file to read.
 * @param errors A pointer to an array of errors to be returned.
 * @param size This will get overwritten with the number of errors.
 * @return 0 for success, something else if an error occurs.
 */
int8_t get_record_errors(char **records, uint32_t num,
                         const uint8_t *error_filename, char ***errors,
                         uint32_t *size) {
  char **all_errors = NULL;
  uint32_t num_errors = 0;
  uint32_t current_buffer_size = INITIAL_BUFFER_SIZE;
  uint32_t i = 0;
  uint32_t j = 0;
  uint32_t k = 0;
  int8_t res = 0;
  FILE *error_file = calloc(1, sizeof(FILE));

  // Malloc first so we can always free outside this function
  (*errors) = malloc(current_buffer_size * sizeof(uint8_t *));

  // If there are no records, we don't care about the errors since no
  // comparision is going to take place
  if (!num) {
    // Just exit gracefully
    return 0;
  }

  all_errors = malloc(current_buffer_size * sizeof(uint8_t *));

  // This simply reads a file and reports all the lines that don't start with
  // '#'
  errno = 0;
  res = get_record_list(error_filename, &all_errors, &num_errors);

  if (0 != res) {
    fprintf(stderr, "IO error when reading error file - 1.\n");
    if (ENOENT != errno) {
      exit(IO_ERROR);
    }
  }

  current_buffer_size = INITIAL_BUFFER_SIZE;

  // Now save only the errors we care about
  for (i = 0; i < num; i++) {
    for (j = 0; j < num_errors; j++) {
      if (!strncmp(records[i], all_errors[j], strlen(records[i]))) {
        // Save that record error
        (*errors)[k++] = my_strdup(all_errors[j]);
      }
    }
  }

  *size = k;

  // We don't care about the local malloc
  free(all_errors);

  return 0;
}

/*
 * Puppetmaster for the statistics functions bxb (beat-by-beat) and rxr
 * (run-by-run) Sets up the database and reads it, then makes calls to bxb and
 * rxr to have the analysis done for a single record. The statistics are then
 * written out to a file.
 */
int32_t stat_analysis(const char *ofname, const char *annotator,
                      const char *reference_annotator,
                      const char *record_filename, const char *error_filename) {
  int32_t counter;
  uint32_t current_error = 0;

  char **records;
  char **record_errors;
  int32_t res = 0;
  char database_path[STR_BUFFER_SIZE];

  char *sfname = NULL; /* filenames for reports */
  uint32_t j = 0;
  uint32_t num_records = 0;
  uint32_t num_errors = 0;

  char ecg_database_path[STR_BUFFER_SIZE];
  ecg_database_path[0] = '\0';
  my_strlcat(ecg_database_path, ".", STR_BUFFER_SIZE);

  // These structs are temporary holders for the data from each record.
  bxb_stats_t *record_bxb_stats;
  rxr_stats_t *record_rxr_stats;

  bxb_stats_t total_bxb_stats;
  memset(&total_bxb_stats, 0, sizeof(bxb_stats_t));

  // update according to the name of database, supplied as
  // an argument

  res = get_record_list(record_filename, &records, &num_records);
  if (0 != res) {
    fprintf(stderr, "IO error when reading record file (%s) - 2.\n",
            record_filename);
    exit(IO_ERROR);
  }
  res = get_record_errors(records, num_records, error_filename, &record_errors,
                          &num_errors);

  // Set up path to database directory
  setwfdb(ecg_database_path);

  // if there is a file name specified, delete the file (if it exists) and open
  // a new one
  if (NULL != ofname) {
    unlink(ofname); // Delete the file if it already exists.
    if ((ofile = fopen(ofname, "w")) == NULL) {
      (void)fprintf(stderr, "can't modify %s\n", ofname);
      exit(IO_ERROR);
    }
  }
  // No file name, set output to stdout
  else {
    ofile = stdout;
  }

  if (record_info) {
    (void)fprintf(ofile, "Beat detection and classification performance\n\n");
    (void)fprintf(
        ofile,
        " Record          Nn     Sn     Vn     Fn     On     Ns     Ss     Vs  "
        "   Fs     Os     Nv     Sv     Vv     Fv     Ov     No     So     Vo  "
        "   Fo    Q Se   Q +P   V Se   V +P  V FPR   S Se   S +P\n");
  }

  // Analyze records in the given directory
  for (counter = 0; counter < num_records; ++counter) {
    sprintf(record, "%s", records[counter]);
    // printf("Record %d\n",Records->at(recNum));
    // sprintf(record, "%s", test_record_str);

    // If there is another record error, and the current record is the one with
    // the error
    if ((current_error < num_errors) &&
        (!strncmp(record, record_errors[current_error], strlen(record)))) {
      // Report the full error in the output file instead of calculating the
      // beat by beat data
      fprintf(ofile, "%s\n", record_errors[current_error++]);
      continue;
    }

    char testAnnName[20], rec;

    sampfreq(record);
    an[0].name = (char *)reference_annotator;
    an[1].name = (char *)annotator;
    an[0].stat = an[1].stat = WFDB_READ;
    match_dt = (int)strtim(".15"); /* 150 milliseconds */
    start = strtim(LEARNING_TIME); /* Originally 5 minutes */
    end_time = -1L;                /* go to end of reference annotation file */
    shut_down = (int)strtim(".5"); /* 1/2 second */

    if (annopen(record, an, 2) < 0) {
      fprintf(stderr, "Couldn't open annotation files.\n");
      exit(IO_ERROR);
    }

    sfile = stdout;
    record_bxb_stats = bxb(ofile, record);
    // res = rxr(ofile, record);
    if (SUCCESS != res) {
      switch (res) {
      case IO_ERROR:
        fprintf(stderr, "Error: IO error with record %s.\n", record);
        break;
      case ANNOTATION_FILE_EMPTY:
        fprintf(stderr, "Error: Annotation %s did not contain any entries\n.",
                record);
        break;
      case ANNOTATION_FILE_BAD_TIME:
        fprintf(stderr,
                "Error: Annotation %s did not contain the desired time.",
                record);
        break;
      default:
        fprintf(stderr, "Unknown error while processing %s.", record);
        break;
      }
      exit(res);
    }

    // long QTP, QFN, QFP, STP, SFN, STN, SFP, VTP, VFN, VTN, VFP;
    total_bxb_stats.QTP += record_bxb_stats->QTP;
    total_bxb_stats.QFN += record_bxb_stats->QFN;
    total_bxb_stats.QFP += record_bxb_stats->QFP;
    total_bxb_stats.STP += record_bxb_stats->STP;
    total_bxb_stats.SFN += record_bxb_stats->SFN;
    total_bxb_stats.STN += record_bxb_stats->STN;
    total_bxb_stats.SFP += record_bxb_stats->SFP;
    total_bxb_stats.VTP += record_bxb_stats->VTP;
    total_bxb_stats.VFN += record_bxb_stats->VFN;
    total_bxb_stats.VTN += record_bxb_stats->VTN;
    total_bxb_stats.VFP += record_bxb_stats->VFP;
    total_bxb_stats.Nn += record_bxb_stats->Nn;
    total_bxb_stats.Sn += record_bxb_stats->Sn;
    total_bxb_stats.Vn += record_bxb_stats->Vn;
    total_bxb_stats.Fn += record_bxb_stats->Fn;
    total_bxb_stats.On += record_bxb_stats->On;
    total_bxb_stats.Ns += record_bxb_stats->Ns;
    total_bxb_stats.Ss += record_bxb_stats->Ss;
    total_bxb_stats.Vs += record_bxb_stats->Vs;
    total_bxb_stats.Fs += record_bxb_stats->Fs;
    total_bxb_stats.Os += record_bxb_stats->Os;
    total_bxb_stats.Nv += record_bxb_stats->Nv;
    total_bxb_stats.Sv += record_bxb_stats->Sv;
    total_bxb_stats.Vv += record_bxb_stats->Vv;
    total_bxb_stats.Fv += record_bxb_stats->Fv;
    total_bxb_stats.Ov += record_bxb_stats->Ov;
    total_bxb_stats.No += record_bxb_stats->No;
    total_bxb_stats.So += record_bxb_stats->So;
    total_bxb_stats.Vo += record_bxb_stats->Vo;
    total_bxb_stats.Fo += record_bxb_stats->Fo;

    if (verbose >= 1) {
      printf("record: %s, record QFP: %d, total QFP: %d\n", record,
             record_bxb_stats->QFP, total_bxb_stats.QFP);
    }
    free(record_bxb_stats);

    // Only do this the last time. Because of the way the file was already
    // structured (constantly closing the file), this was easier than
    // putting it after the loop.
    if (counter + 1 == num_records) {
      print_total_stats(ofile, &total_bxb_stats);
    }

    wfdbquit(); /* close input files */
  }

  if (ofile != NULL) {
    fclose(ofile);
  }

  return 0;
}

size_t my_strlcat(char *dst, const char *src, size_t siz) {
  char *d = dst;
  const char *s = src;
  size_t n = siz;
  size_t dlen;

  /* Find the end of dst and adjust bytes left but don't go past end */
  while (n-- != 0 && *d != '\0')
    d++;
  dlen = d - dst;
  n = siz - dlen;

  if (n == 0)
    return (dlen + strlen(s));
  while (*s != '\0') {
    if (n != 1) {
      *d++ = *s;
      n--;
    }
    s++;
  }
  *d = '\0';

  return (dlen + (s - src)); /* count does not include NUL */
}
