/* file: bxb.c	G. Moody	14 December 1987
Revised:	 7 November 2001
Revised: 5/13/2002 -- Patrick Hamilton
Revised:	10 April 2003 (GBM)
-------------------------------------------------------------------------------
bxb: ANSI/AAMI-standard beat-by-beat annotation file comparator
Copyright (C) 2001 George B. Moody

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA.

You may contact the author by e-mail (george@mit.edu) or postal mail
(MIT Room E25-505A, Cambridge, MA 02139 USA).  For updates to this software,
please visit PhysioNet (http://www.physionet.org/).
_______________________________________________________________________________

This program implements the beat-by-beat comparison algorithms described in
AAMI/ANSI EC38:1998, the American National Standard for ambulatory ECGs, and
in AAMI EC57:1998, the American National Standard for Testing and Reporting
Performance Results of Cardiac Rhythm and ST Segment Measurement Algorithms.
These standards are available from AAMI, 1110 N Glebe Road, Suite 220,
Arlington, VA 22201 USA (http://www.aami.org/).

The -f, -O, -t, and -w options modify the comparison algorithm used by bxb in
ways not permitted by EC38:1998 or EC57:1998.  These options are provided for
the use of developers, who may find them useful for obtaining a more detailed
understanding of algorithm errors.

This version of bxb.cpp has been modified to run without command line input,
batch comparing .ate annotations to .atr annotations for MIT/BIH database
or AHA database files.  The test matrices are stored in "testrpt.txt".  The
Record #, QRS TP, QRS FN, QRS FP, PVC TP, PVC FN, and PVC FP are stored in
the file "adstat.txt".

 */

#include <sys/stat.h>
//#include <memory>
#include <malloc.h>
//#include <cmath>
#include "stat_analysis.h"

#include <stdlib.h>	// For exit.
#include <math.h>	/* for declaration of sqrt() */

#ifndef map1
#define map1
#endif

#ifndef map2
#define map2
#endif

#ifndef ammap
#define ammap
#endif

#ifndef mamap
#define mamap
#endif

#ifndef annpos
#define annpos
#endif

#define abs(A)	((A) >= 0 ? (A) : -(A))

// Local functions
int32_t rpann(long t);
int32_t tpann(long t);
void pair(int32_t ref, int32_t test);
void bxb_pstat(FILE *ofile, char *f, long a, long b);
void sstat(FILE *sfile, char *s, char *f, long a, long b);
bxb_stats_t* bxb_print_results(FILE *ofile, char* record);
int32_t bxb_amap(int32_t a);

// Local variables

// ref annot vars
long RR; /* reference RR interval, if non-zero */
long sdonref = -1L; /* start of reference shutdown */
long sdoffref = -1L; /* end of reference shutdown */
long vfonref = -1L; /* start of reference VF */
long vfoffref = -1L; /* end of reference VF */
long psdonref = -1L; /* start of previous reference shutdown */
long psdoffref = -1L; /* end of previous reference shutdown */
long pvfonref = -1L; /* start of previous reference VF */
long pvfoffref = -1L; /* end of previous reference VF */

// test annot vars
long rr; /* test RR interval, if non-zero */
long sdontest = -1L; /* start of test shutdown */
long sdofftest = -1L; /* end of test shutdown */
long vfontest = -1L; /* start of test VF */
long vfofftest = -1L; /* end of test VF */
long psdontest = -1L; /* start of previous test shutdown */
long psdofftest = -1L; /* end of previous test shutdown */
long pvfontest = -1L; /* start of previous test VF */
long pvfofftest = -1L; /* end of previous test VF */

// confusion matrix vars
static long Nn, Ns, Nv, Nf, Nq, No, Nx,
Sn, Ss, Sv, Sf, Sq, So, Sx,
Vn, Vs, Vv, Vf, Vq, Vo, Vx,
Fn, Fs, Fv, Ff, Fq, Fo, Fx,
Qn, Qs, Qv, Qf, Qq, Qo, Qx,
On, Os, Ov, Of, Oq,
Xn, Xs, Xv, Xf, Xq;
long nrre = 0; /* number of RR errors tallied in ssrre */
double ssrre = 0.; /* sum of squares of RR errors */

/* getref() and gettest() read the next beat annotations from their respective
   files. */
void getref(void) /* get next reference beat annotation */
{
    static long TT; /* time of previous reference beat annotation */
    static struct WFDB_ann annot;

    TT = T;
    T = Tprime;
    A = Aprime;

    /* T-TT is not a valid RR interval if T is the time of the first beat,
       if TT is the time of the last beat, or if a period of VF or shutdown
       occurs between TT and T. */
    if (TT == 0L || T == huge_time ||
            (TT <= vfonref && vfonref < T) ||
            (TT <= sdonref && sdonref < T) ||
            (TT <= pvfonref && pvfonref < T) ||
            (TT <= psdonref && psdonref < T))
    {
        RR = 0L;
    }

    else
    {
        RR = T - TT;
    }

    if (oflag)
    {
        ref_annot = annot;
    }

    /* Read reference annotations until a beat annotation is read, or EOF.
       If an expanded output annotation file is required, all annotations
       are treated as if they were beat annotations. */
    while (getann(0, &annot) == 0)
    {
        if (isqrs(annot.anntyp))
        { /* beat annotation */
            Tprime = annot.time;
            Aprime = bxb_amap(annot.anntyp);
            return;
        }

            /* Shutdown occurs when neither signal is readable;  the beginning of
               shutdown is indicated by a NOISE annotation in which bits 4 and 5
               of the subtyp field are set, and the end of shutdown is indicated
               by a NOISE annotation with any value of `subtyp' for which at least
               one of bits 4 and 5 is zero.  In AHA DB reference annotation files,
               shutdown is indicated by a single shutdown annotation placed roughly
               in the middle of the shutdown interval;  in this case, shutdown is
               assumed to begin match_dt samples after the previous beat annotation
               or VFOFF annotation, and is assumed to end match_dt samples before
               the next annotation.
             */

        else if (annot.anntyp == NOISE)
        {
            if ((annot.subtyp & 0x30) == 0x30)
            {
                psdonref = sdonref;
                psdoffref = sdoffref;
                sdonref = annot.time;

                /* Read next annotation, which should mark end of shutdown. */
                if (getann(0, &annot) < 0)
                { /* EOF before end of shutdown */
                    Tprime = sdoffref = huge_time;
                    Aprime = '*';
                    return;
                }

                if (annot.anntyp == NOISE && (annot.subtyp & 0x30) != 0x30)
                {
                    sdoffref = annot.time;
                }

                else
                {
                    if (vfoffref > T)
                    {
                        sdonref = vfoffref + match_dt;
                    }

                    else
                    {
                        sdonref = T + match_dt;
                    }

                    sdoffref = annot.time - match_dt;

                    if (sdonref > sdoffref)
                    {
                        sdonref = sdoffref;
                    }

                    (void) ungetann(0, &annot);
                }
            }
        }

            /* The beginning of ventricular fibrillation is indicated by a VFON
               annotation, and its end by a VFOFF annotation;  any annotations
               between VFON and VFOFF are read and ignored. */
        else if (annot.anntyp == VFON)
        {
            pvfonref = vfonref;
            pvfoffref = vfoffref;
            vfonref = annot.time;

            /* Read additional annotations, until end of VF or EOF. */
            do
            {
                if (getann(0, &annot) < 0)
                { /* EOF before end of VF */
                    Tprime = huge_time;
                    Aprime = '*';
                    return;
                }
            }
            while (annot.anntyp != VFOFF);

            vfoffref = annot.time;
        }
    }

    /* When this statement is reached, there are no more annotations in the
       reference annotation file. */
    Tprime = huge_time;
    Aprime = '*';
}

void gettest(char* record) /* get next test annotation */
{
    static long tt; /* time of previous test beat annotation */
    static struct WFDB_ann annot;

    tt = t;
    t = tprime;
    a = aprime;

    /* See comments on the similar code in getref(), above. */
    if (tt == 0L || t == huge_time ||
            (tt <= vfontest && vfontest < t) ||
            (tt <= sdontest && sdontest < t) ||
            (tt <= pvfontest && pvfontest < t) ||
            (tt <= psdontest && psdontest < t))
    {
        rr = 0L;
    }

    else
    {
        rr = t - tt;
    }

    if (oflag)
    {
        test_annot = annot;
    }

    while (getann(1, &annot) == 0)
    {
        if (isqrs(annot.anntyp))
        {
            tprime = annot.time;
            aprime = bxb_amap(annot.anntyp);
            return;
        }

        if (annot.anntyp == NOISE)
        {
            if ((annot.subtyp & 0x30) == 0x30)
            {
                psdontest = sdontest;
                psdofftest = sdofftest;
                sdontest = annot.time;

                if (getann(1, &annot) < 0)
                {
                    tprime = huge_time;
                    aprime = '*';

                    if (end_time > 0L)
                    {
                        shut_down += end_time - sdontest;
                    }

                    else
                    {
                        (void) fprintf(stderr,
                                       "unterminated shutdown starting at %s in record %s, annotator %s\n", timstr(sdontest), record, an[1].name);
                        (void) fprintf(stderr,
                                       " (not included in shutdown duration measurement)\n");
                    }

                    return;
                }

                if (annot.anntyp == NOISE && (annot.subtyp & 0x30) != 0x30)
                {
                    sdofftest = annot.time;
                }

                else
                {
                    if (vfofftest > t)
                    {
                        sdontest = vfofftest + match_dt;
                    }

                    else
                    {
                        sdontest = t + match_dt;
                    }

                    sdofftest = annot.time - match_dt;

                    if (sdontest > sdofftest)
                    {
                        sdontest = sdofftest;
                    }

                    (void) ungetann(0, &annot);
                }

                /* update shutdown duration tally */
                shut_down += sdofftest - sdontest;
            }
        }

        else if (annot.anntyp == VFON)
        {
            pvfontest = vfontest;
            pvfofftest = vfofftest;
            vfontest = annot.time;

            do
            {
                if (getann(1, &annot) < 0)
                {
                    tprime = huge_time;
                    aprime = '*';
                    return;
                }
            }
            while (annot.anntyp != VFOFF);

            vfofftest = annot.time;
        }
    }

    tprime = huge_time;
    aprime = '*';
}

/* Functions rpann() and tpann() return the appropriate pseudo-beat label
   for the time specified by their argument.  They should be called only
   with time arguments which match the times of the current test or reference
   beat labels, since they depend on getref() and gettest() to locate the two
   most recent VF and shutdown periods and have no information about earlier
   or later VF or shutdown periods. */
int32_t rpann(long t)
{
    if ((vfonref != -1L && vfonref <= t && (t <= vfoffref || vfoffref == -1L)) ||
            (pvfonref != -1L && pvfonref <= t && t <= pvfoffref))
    {
        return ('*'); /* test beat labels during reference-marked VF are
                           not to be counted;  since `*' is not recognized by
                           pair(), returning `*' accomplishes this */
    }

    else if ((sdonref != -1L && sdonref <= t && (t <= sdoffref || sdoffref == -1L)) ||
            (psdonref != -1L && psdonref <= t && t <= psdoffref))
    {
        return ('X'); /* test beat labels during reference-marked shutdown
                           are paired with X pseudo-beat labels */
    }

    else
    {
        return ('O'); /* all other extra test beat labels are paired with
                           O pseudo-beat labels */
    }
}

int32_t tpann(long t)
{
    /* no special treatment for reference beat labels during test-marked VF */
    if ((sdontest != -1L && sdontest <= t && (t <= sdofftest || sdofftest == -1L)) ||
            (psdontest != -1L && psdontest <= t && t <= psdoffref))
    {
        return ('X'); /* reference beat labels during test-marked shutdown
                           are paired with X pseudo-beat labels */
    }

    else
    {
        return ('O'); /* all other extra reference beat labels are paired
                           with O pseudo-beat labels */
    }
}

/* Define counters for the elements of the confusion matrix.  Static variables
   have initial values of zero.  

   Confusion matrix keeps track of physician annotated beat (ref) vs algorithm-generated beat (test) 
Ex: if the ref beat is classified as a normal beat (N) and the test beat is classified as 
a ventricular beat (V), then a count of 1 would be added to the count of Nv */
void pair(int32_t ref, int32_t test) /* count a beat label pair */
{
    switch (ref)
    {
    case 'N':
        switch (test)
        {
        case 'N': Nn++;
            break;
        case 'S': Ns++;
            break;
        case 'V': Nv++;
            break;
        case 'F': Nf++;
            break;
        case 'Q': Nq++;
            break;
        case 'O': No++;
            break;
        case 'X': Nx++;
            break;
        }
        break;
    case 'S':
        switch (test)
        {
        case 'N': Sn++;
            break;
        case 'S': Ss++;
            break;
        case 'V': Sv++;
            break;
        case 'F': Sf++;
            break;
        case 'Q': Sq++;
            break;
        case 'O': So++;
            break;
        case 'X': Sx++;
            break;
        }
        break;
    case 'V':
        switch (test)
        {
        case 'N': Vn++;
            break;
        case 'S': Vs++;
            break;
        case 'V': Vv++;
            break;
        case 'F': Vf++;
            break;
        case 'Q': Vq++;
            break;
        case 'O': Vo++;
            break;
        case 'X': Vx++;
            break;
        }
        break;
    case 'F':
        switch (test)
        {
        case 'N': Fn++;
            break;
        case 'S': Fs++;
            break;
        case 'V': Fv++;
            break;
        case 'F': Ff++;
            break;
        case 'Q': Fq++;
            break;
        case 'O': Fo++;
            break;
        case 'X': Fx++;
            break;
        }
        break;
    case 'Q':
        switch (test)
        {
        case 'N': Qn++;
            break;
        case 'S': Qs++;
            break;
        case 'V': Qv++;
            break;
        case 'F': Qf++;
            break;
        case 'Q': Qq++;
            break;
        case 'O': Qo++;
            break;
        case 'X': Qx++;
            break;
        }
        break;
    case 'O':
        switch (test)
        {
        case 'N': On++;
            break;
        case 'S': Os++;
            break;
        case 'V': Ov++;
            break;
        case 'F': Of++;
            break;
        case 'Q': Oq++;
            break;
        }
        break;
    case 'X':
        switch (test)
        {
        case 'N': Xn++;
            break;
        case 'S': Xs++;
            break;
        case 'V': Xv++;
            break;
        case 'F': Xf++;
            break;
        case 'Q': Xq++;
            break;
        }
        break;
    }

    /* Compute the RR interval error and update the sum of squared errors. */
    if (RR > 0L && rr > 0L)
    {
        double rre = RR - rr;
        ssrre += rre*rre;
        nrre++;
    }

    // if it's specified to make an updated output annotation file
    // with some sort of commentary on the incorrect/missed beats
    if (oflag)
    {
        if (ref == test)
        {
            (void) putann(0, &test_annot);
        }

        else
        {
            struct WFDB_ann out_annot;
            char auxp[3];

            auxp[0] = 2;
            auxp[1] = ref;
            auxp[2] = test - 'A' + 'a';
            if (test == 'O' || test == 'X')
            {
                out_annot.time = T;
            }

            else
            {
                out_annot.time = t;
            }

            out_annot.anntyp = NOTE;
            out_annot.subtyp = out_annot.chan = out_annot.num = 0;
            out_annot.aux = (unsigned char*) auxp;
            (void) putann(0, &out_annot);
        }
    }

    // if we want more details on the incorrect/missed beats
    if ((verbose >= 2) && ref != test)
    {
        if (ref == 'O' || ref == 'X')
        {
            (void) fprintf(stderr, "%c(%ld)/%c(%ld)\n", ref, t, test, t);
        }

        else if (test == 'O' || test == 'X')
        {
            (void) fprintf(stderr, "%c(%ld)/%c(%ld)\n", ref, T, test, T);
        }

        else
        {
            (void) fprintf(stderr, "%c(%ld)/%c(%ld)\n", ref, T, test, t);
        }
    }
}

/* 'bxb_pstat' prints a statistic that is the quotient of a and b expressed in percentage units.  
   No-data values are indicated by '-'. 

   This is a separate function from sstat most likely for language reasons. 
   It prints to ofile instead of sfile which are declared outside the function. */
void bxb_pstat(FILE *ofile, char *f, long a, long b)
{
    // if there is no data for this particular statistic, then just print a dash
    if (b <= 0)
    {
        (void) fprintf(ofile, "     - ");
    }
        // otherwise, calculate statistic and use the given format for printing to file
    else
    {
        (void) fprintf(ofile, f, (100. * a) / b);
    }
}

/* 'sstat' prints a statistic described by the sentence passed through s. 
   The statistic is the quotient of a and b expressed in percentage units.  
   No-data values are indicated by '-'. 

   This is a separate function from bxb_pstat most likely for language reasons. 
   It prints to sfile instead of ofile which are declared outside the function. */
void sstat(FILE *sfile, char *s, char *f, long a, long b)
{
    // print headings
    (void) fprintf(sfile, "%s: ", s);
    // if there is no data for this particular statistic, then just print a dash
    if (b <= 0)
    {
        (void) fprintf(sfile, "     - ");
    }
        // otherwise, calculate statistic and use the given format for printing to file
    else
    {
        (void) fprintf(sfile, f, (100. * a) / b);
        (void) fprintf(sfile, "%%");
    }
    // print how the calculation was made
    (void) fprintf(sfile, " (%ld/%ld)\n", a, b);
}

int32_t bxb_amap(int32_t a) /* map MIT annotation code into AAMI test label */
{
    switch (a)
    {
    case NORMAL:
    case LBBB:
    case RBBB:
    case BBB: return ('N');
    case NPC:
    case APC:
    case SVPB:
    case ABERR:
    case NESC:
    case AESC:
    case SVESC: return ('S');
    case PVC:
    case RONT:
    case VESC: return ('V');
    case FUSION: return ('F');
    case UNKNOWN: return ('Q');

        /* The AAMI RP excludes records containing paced beats from its reporting
           requirements.  To permit this program to be used with such records,
           beats which are either paced (type PACE) or fusions of paced and normal
           beats (type PFUS) are treated in the same way as unknown beats. */
    case PACE:
    case PFUS: return ('Q');

        /* LEARN annotations should appear only in the `test' annotation file, and
           only during the learning period;  if they appear elsewhere, they are
           treated in the same way as unknown beats. */
    case LEARN: return ('Q');

        /* Other annotations (including NOISE and VFON/VFOFF) are treated as non-beat
           annotations. */
    default: return ('O');
    }
}

bxb_stats_t* bxb_print_results(FILE *ofile, char* record)
{
    // long QTP, QFN, QFP, STP, SFN, STN, SFP, VTP, VFN, VTN, VFP;
    bxb_stats_t* stats = (bxb_stats_t*) calloc(1, sizeof (bxb_stats_t));

    // calculate stats: true positive, false negative, false positive, true negative for 
    // QRS, VEB (ventricular), and SVEB (supraventricular)
    stats->QTP = Nn + Ns + Nv + Nf + Nq + Sn + Ss + Sv + Sf + Sq + Vn + Vs + Vv + Vf + Vq + Fn + Fs + Fv + Ff + Fq + Qn + Qs + Qv + Qf + Qq;
    stats->QFN = No + Nx + So + Sx + Vo + Vx + Fo + Fx + Qo + Qx;
    stats->QFP = On + Os + Ov + Of + Oq + Xn + Xs + Xv + Xf + Xq;
    stats->VTP = Vv;
    stats->VFN = Vn + Vs + Vf + Vq + Vo + Vx;
    stats->VTN = Nn + Ns + Nf + Nq + Sn + Ss + Sf + Sq + Fn + Fs + Ff + Fq + Qn + Qs + Qf + Qq + On + Os + Of + Oq + Xn + Xs + Xf + Xq;
    stats->VFP = Nv + Sv + Ov + Xv;
    stats->STP = Ss;
    stats->SFN = Sn + Sv + Sf + Sq + So + Sx;
    stats->STN = Nn + Nv + Nf + Nq + Vn + Vv + Vf + Vq + Fn + Fv + Ff + Fq + Qn + Qv + Qf + Qq + On + Ov + Of + Oq + Xn + Xv + Xf + Xq;
    stats->SFP = Ns + Vs + Fs + Os + Xs;
    stats->Nn = Nn;
    stats->Sn = Sn;
    stats->Vn = Vn;
    stats->Fn = Fn;
    stats->On = On;
    stats->Ns = Ns;
    stats->Ss = Ss;
    stats->Vs = Vs;
    stats->Fs = Fs;
    stats->Os = Os;
    stats->Nv = Nv;
    stats->Sv = Sv;
    stats->Vv = Vv;
    stats->Fv = Fv;
    stats->Ov = Ov;
    stats->No = No;
    stats->So = So;
    stats->Vo = Vo;
    stats->Fo = Fo;

    if (record_info)
    {
        (void) fprintf(ofile, "%-10s   %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld %6ld ",
                    record, Nn, Sn, Vn, Fn, On, Ns, Ss, Vs, Fs, Os, Nv, Sv, Vv, Fv, Ov, No, So, Vo, Fo);
        bxb_pstat(ofile, " %6.2f", stats->QTP, stats->QTP + stats->QFN); //QRS sensitivity
        bxb_pstat(ofile, " %6.2f", stats->QTP, stats->QTP + stats->QFP); //QRS positive predictivity
        bxb_pstat(ofile, " %6.2f", stats->VTP, stats->VTP + stats->VFN); //VEB sensitivity
        bxb_pstat(ofile, " %6.2f", stats->VTP, stats->VTP + stats->VFP); //VEB positive predictivity
        bxb_pstat(ofile, " %6.3f", stats->VFP, stats->VTN + stats->VFP); //VEB false positive rate
        bxb_pstat(ofile, " %6.2f", stats->STP, stats->STP + stats->SFN); //SVEB sensitivity
        bxb_pstat(ofile, " %6.2f", stats->STP, stats->STP + stats->SFP); //SVEB positive predictivity
        (void) fprintf(ofile, "\n");
    }

    return stats;
}

/**
 ** Puppetmaster for beat-by-beat comparisons
 **/
bxb_stats_t* bxb(FILE *ofile, char* record)
{
    // Initialize counts.
    Nn = Ns = Nv = Nf = Nq = No = Nx = 0;
    Sn = Ss = Sv = Sf = Sq = So = Sx = 0;
    Vn = Vs = Vv = Vf = Vq = Vo = Vx = 0;
    Fn = Fs = Fv = Ff = Fq = Fo = Fx = 0;
    Qn = Qs = Qv = Qf = Qq = Qo = Qx = 0;
    On = Os = Ov = Of = Oq = 0;
    Xn = Xs = Xv = Xf = Xq = 0;

    T = Tprime = 0;
    t = tprime = 0;

    /* Set A and T to the type and time of the first reference annotation after
       the end of the learning period. */
    do
    {
        getref();
    }
    while (T < start);

    /* Set aprime and tprime to the type and time of the first test annotation
       after the end of the learning period, and a and t to the type and time
       of the last test annotation in the learning period. */
    do
    {
        gettest(record);
    }
    while (tprime < start);

    /* If t matches the first reference annotation, count it and get the next
       annotation from each file.  (Since T >= start and t < start, T-t must be
       positive.) */
    if (T - t < abs(T - tprime) && T - t <= match_dt)
    {
        if (A != 0 || a != 0) /* false only if start = 0 */
        {
            pair(A, a);
        }
        getref();
        gettest(record);
    }

        /* If there is a test annotation within an interval equal to the match
           window following the beginning of the test period, and there is no
           match, go on to the next test annotation without counting the first
           one. */
    else
    {
        gettest(record);
        if (t - start <= match_dt && abs(T - tprime) < abs(T - t))
        {
            gettest(record);
        }
    }

    /* Peform the comparison.  Each time through the loop below, a beat label
       pair is identified and counted (or else a non-beat annotation is
       discarded), and an annotation is read from each file from which an
       annotation was paired or discarded.  Note that only one of the four
       numbered actions is performed on each iteration.

       The complex loop termination condition is dependent on end_time, which
       is not changed during execution of the loop.  There are three ways the
       loop termination condition can be satisfied:
       - If the length of the comparison is known, either because it was
       specified using the `-t' option or because the header file specifies
       the record length, the loop ends when both T and t are greater than
       end_time.  This is the usual case.
       - If the length of the comparison is unknown (end_time = -1), the loop
       ends when EOF is reached in the reference annotation file (T =
       huge_time).
       - If the option `-t 0' was specified (end_time = 0), the loop ends when
       EOF is first reached in either annotation file (T or t = huge_time).
     */
    while ((end_time > 0L && (T <= end_time || t <= end_time)) ||
            (end_time == -1L && T != huge_time) ||
            (end_time == 0L && T != huge_time && t != huge_time))
    {
        if (t < T)
        { /* test annotation is earliest */

            /* (1) If t is within the match window, and is a better match than
               the next test annotation, pair it. */
            if (T - t <= match_dt && T - t < abs(T - tprime))
            {
                pair(A, a);
                getref();
                gettest(record);
            }

                /* (2) There is no match to the test annotation, so pair it with a
                   pseudo-beat annotation and get the next one. */
            else
            {
                pair(rpann(t), a);
                gettest(record);
            }
        }

        else
        { /* reference annotation is earliest */

            /* (3) If T is within the match window, and is a better match than
               the next reference annotation, pair it. */
            if (t - T <= match_dt && t - T < abs(t - Tprime))
            {
                pair(A, a);
                gettest(record);
                getref();
            }

                /* (4) There is no match to the reference annotation, so pair it
                   with a pseudo-beat annotation and get the next one. */
            else
            {
                pair(A, tpann(T));
                getref();
            }
        }
    }

    shut_down /= strtim("1"); /* convert from samples to seconds */

    // Generate output and return the numerical results.
    return bxb_print_results(ofile, record);
}
