# Makefile to build the heartbeat analysis algorithm for TZ Medical

CC=gcc
CFLAGS= -std=c99
OUTFILE= src/h3rAlgorithmStats/stat_analysis
INCLUDE_DIRS= -I/usr/local/include
LIBRARY_DIRS= -L"/usr/local" -L"/usr/local/lib" -L"/usr/local/lib64" -L"/usr/local/bin" -L"/usr/local/include"
LIBRARIES= -lwfdb
INPUT_FILES= ext/main.c ext/stat_analysis.c ext/bxb.c ext/rxr.c

all: clean tz_algorithm

clean:
	@if [ -e src/h3rAlgorithmStats/stat_analysis.exe ]; then rm src/h3rAlgorithmStats/stat_analysis.exe; fi
	@if [ -e src/h3rAlgorithmStats/stat_analysis ]; then rm src/h3rAlgorithmStats/stat_analysis; fi
	@if [ -e /usr/local/bin/calc_stats ]; then rm /usr/local/bin/calc_stats; fi

tz_algorithm:
	$(CC) $(CFLAGS) $(INCLUDE_DIRS) $(LIBRARY_DIRS) -o $(OUTFILE) $(INPUT_FILES) $(LIBRARIES) 



