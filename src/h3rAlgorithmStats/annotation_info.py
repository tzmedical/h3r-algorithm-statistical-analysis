class AnnotationLabel:
    '''
    Basically an enum.
    These strings are used to denote tachycardia, bradycardia, etc in Rhythm Annotation files.
    '''
    TACHY = "t"
    PSEUDO_TACHY = "T"
    BRADY = "b"
    PSEUDO_BRADY = "B"
    AF = "a"
    PAUSE = "p"
    PSEUDO_PAUSE = "P"
    NORMAL = "N"
    NSR = "n"
    AFL = "F"
    NODAL = "J" # Nodal (junctional) premature beat
    PVC = "V"
    SVEB = "S"
    UNKNOWN = "Q"
    UNANALYZABLE = "U"

    def __iter__(self):
        return (getattr(self, x) for x in dir(self) if not x.startswith('__') and not x == 'NORMAL')
    
class AnnotationName:
    '''
    Basically an enum.
    This array is used to print the names tachycardia, bradycardia, etc in Rhythm Annotation files.
    '''   
    names = {}
    names["t"] = "Tachycardia"
    names["b"] = "Bradycardia"
    names["a"] = "AF"
    names["p"] = "Pause"
    names["n"] = "Normal" # Calling the NSR label "Normal" 
    names["U"] = "Unanalyzable"

"""
# This dictionary translates WFDB symbols to those found in AnnotationLabel.py
# wfdb symbols can be found here: http://www.physionet.org/physiobank/annotations.shtml
WFDB_ECG_Codes = {
    'N':AnnotationLabel.NSR,
    'AFIB':AnnotationLabel.AF,
    'AFL':AnnotationLabel.AFL,
    'J':AnnotationLabel.NODAL,
    'S':AnnotationLabel.SVEB,
    'V':AnnotationLabel.PVC,
    'Q':AnnotationLabel.UNKNOWN
}
"""

