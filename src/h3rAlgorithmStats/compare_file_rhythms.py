#!/usr/bin/env python3
#
# Author: Jeremy Sigrist
#
# Rhythm files must be in the format:
# Record  Type    Start   Stop    Frequency
# 101     BC      103     1032    360
# 101     PS      2103    3032    360
# 102     TC      101     1002    360
#
# This program also requires that the wfdb library be installed:
# http://www.physionet.org/physiotools/wfdb.shtml

import sys
import argparse
from collections import defaultdict

labels = ["A", "B", "P", "T"]

# How many samples off can a timestamp be from the reference while still
# being considered the same beat?
# How far away from an rhythm can a beat be while still being classified as part of the rhythm?
# e.g. if the reference denotes pause ending at 1000 samples and the algorithm ends it on the
# same beat at 1002 samples, does that still count as a false positive?
# 18 samples = 50 ms at 360 samples / second
RHYTHM_TOLERANCE = 18

# What was the training time allotted to the algorithm at the beginning of the record?
# Units are minutes, decimals are supported.
TRAINING_TIME = 2


class RhythmStats:
    """
    This class simply acts as a convenient container for the different rhythm stats associated
    with a given type of anomaly.
    """

    def __init__(self):
        """
        Contains true positives, false positives, true negatives, and false negatives.
        """
        self.fp = 0
        self.fn = 0
        self.tp = 0
        self.tn = 0

    def calculate(self):
        """
        Adds sensitivity, specificity, negative predictivity, and positive predictivity
        to the object.
        """
        self.sensitivity = (
            self.tp / (self.tp + self.fn) if (self.tp + self.fn) > 0 else 0
        )
        self.specificity = (
            self.tn / (self.tn + self.fp) if (self.tn + self.fp) > 0 else 0
        )
        self.npv = self.tn / (self.tn + self.fn) if (self.tn + self.fn) > 0 else 0
        self.ppv = self.tp / (self.tp + self.fp) if (self.tp + self.fp) > 0 else 0

    def avg(self):
        return (self.sensitivity + self.specificity + self.npv + self.ppv) / 4

    def __str__(self):
        """
        Returns a multiline summary of the statistics contained in the object.
        """
        self.calculate()
        return "\t".join(
            [
                "{:.2f}".format(x) if (int(x) != x) else (str(x))
                for x in [
                    self.tp,
                    self.tn,
                    self.fp,
                    self.fn,
                    self.ppv,
                    self.npv,
                    self.sensitivity,
                    self.specificity,
                ]
            ]
        )


def print_stats(stats):
    """
    Print a summary of the statistics.
    """
    print("\t\t", end="")
    print("\t".join(["TP", "TN", "FP", "FN", "PPV", "NPV", "Sn", "Sp"]))
    total_avg = 0
    for label in ["Tachycardia:", "Bradycardia:", "Pause:\t", "AF:\t"]:
        print("{}\t".format(label), end="")
        print(stats[label[0]])
        total_avg += stats[label[0]].avg()
    total_avg /= 4
    print("Total average: {:.2f}".format(total_avg))


def compare_rhythm_annotations(reference_rhythm, algorithm_rhythm, records):
    debug_info = defaultdict(set)
    try:
        with open(records, mode="r") as record_file, open(
            reference_rhythm, mode="r"
        ) as reference_rhythm_file, open(
            algorithm_rhythm, mode="r"
        ) as algorithm_rhythm_file:

            reference = defaultdict(set)
            algorithm = defaultdict(set)
            stats = defaultdict(RhythmStats)
            sample_frequency = 250

            # Skip header line.
            next(reference_rhythm_file)
            for line in reference_rhythm_file:
                line = line.split()
                reference[line[0]].add(line[1])

            next(algorithm_rhythm_file)
            for line in algorithm_rhythm_file:
                line = line.split()
                if (int(line[3]) / sample_frequency) > (TRAINING_TIME * 60):
                    algorithm[line[0]].add(line[1])

            for record in record_file:
                record = record.strip()

                for label in labels:
                    if label in reference[record] and label in algorithm[record]:
                        stats[label].tp += 1
                        debug_info[record].add("{} {}".format(label, "tp"))
                    elif label in reference[record]:
                        stats[label].fn += 1
                        debug_info[record].add("{} {}".format(label, "fn"))
                    elif label in algorithm[record]:
                        stats[label].fp += 1
                        debug_info[record].add("{} {}".format(label, "fp"))
                    else:
                        stats[label].tn += 1
                        debug_info[record].add("{} {}".format(label, "tn"))
            return stats
            try:
                print("{}".format(os.getcwd()))
                with open("../record_stats.txt", mode="w") as outfile:
                    print_debug_info(outfile, debug_info)
            except IOError:
                pass

    except FileNotFoundError as err:
        print("{}: File could not be found.".format(err.filename), file=sys.stderr)
        exit(-1)


def print_debug_info(outfile, debug_info):
    print("R\tA\tB\tP\tT", file=outfile)
    for record in sorted(debug_info):
        print(
            "{} ".format(record)
            + "\t".join(x[-2:] for x in sorted(debug_info[record])),
            file=outfile,
        )


def main():
    """
    Run the program
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-r",
        "--records",
        nargs=1,
        help="The record file for the database",
        metavar="Record_File",
        dest="records",
        required=True,
    )
    parser.add_argument(
        "-rr",
        "--referencerhythm",
        nargs=1,
        help="The rhythm file containing reference annotations",
        metavar="Rhythm_Reference",
        dest="reference_rhythm",
        required=True,
    )
    parser.add_argument(
        "-ar",
        "--algorithmrhythm",
        nargs=1,
        help="The rhythm file containing algorithm annotations",
        metavar="Algorithm_Reference",
        dest="algorithm_rhythm",
        required=True,
    )

    args = parser.parse_args()

    print_stats(
        compare_rhythm_annotations(
            args.reference_rhythm[0], args.algorithm_rhythm[0], args.records[0]
        )
    )


if __name__ == "__main__":
    main()


if __name__ == "__main__":
    pass
