#!/usr/bin/env python3
# This script generates rhythm arrhythmia annotations for the files in the mit database,
# then calculates statistics for how similar the algorithm-generated annotations are
# to the reference annotations.

import sys
import os
import subprocess
import argparse
from h3rAlgorithmStats import gen_rhythm_annotations
from h3rAlgorithmStats import compare_rhythm_annotations
from h3rAlgorithmStats import compare_beat_annotations


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-alg",
        "--alg-annotator",
        type=str,
        help="The algorithm annotator extension",
        metavar="Algorithm Annotator Extension",
        dest="alg_ann",
        required=True,
    )
    parser.add_argument(
        "-genaf",
        "--generate-afib",
        action="store_true",
        help="Generate atrial fibrillation episodes in the reference annotations",
        dest="gen_af",
        required=False,
    )
    parser.add_argument(
        "-nogenaf",
        "--no-generate-afib",
        action="store_false",
        help="Do not generate atrial fibrillation episodes in the reference annotations",
        dest="gen_af",
        required=False,
    )
    parser.set_defaults(gen_af=True)
    parser.add_argument(
        "-ondur",
        "--min-episode-onset-duration",
        type=int,
        help="The minimum episode duration to be included, in ms (default 15000)",
        metavar="Minimum Episode Onset Duration",
        dest="min_episode_onset_duration_ms",
        required=False,
        default=15000,
    )
    parser.add_argument(
        "-exdur",
        "--min-episode-exit-duration",
        type=int,
        help="The minimum duration for an episode to end, in ms (default 15000)",
        metavar="Minimum Episode Exit Duration",
        dest="min_episode_exit_duration_ms",
        required=False,
        default=15000,
    )
    parser.add_argument(
        "-ref",
        "--ref-annotator",
        type=str,
        help="The reference annotator extension (default atr)",
        metavar="Reference Annotator Extension",
        dest="ref_ann",
        required=False,
        default="atr",
    )
    parser.add_argument(
        "-rec",
        "--records",
        type=str,
        help="The records filename (usually RECORDS.txt)",
        metavar="Records Filename",
        dest="record_filename",
        required=False,
        default="RECORDS.txt",
    )
    parser.add_argument(
        "-err",
        "--error-output",
        type=str,
        help="The errors filename containing a list of records with beat-by-beat errors (default Rhythm_Errors_[algorithm annotator].txt)",
        metavar="Errors Filename",
        dest="errors_filename",
        required=False,
        default="Rhythm_Errors_",
    )
    parser.add_argument(
        "-tthr",
        "--tachy-threshold",
        type=int,
        help="The lower threshold for a person to have a tachycardia rhythm",
        metavar="Tachy Threshold",
        dest="tachy_thresh",
        required=False,
        default=100,
    )
    parser.add_argument(
        "-bthr",
        "--brady-threshold",
        type=int,
        help="The upper threshold for a person to have a bradycardia rhythm",
        metavar="Brady Threshold",
        dest="brady_thresh",
        required=False,
        default=60,
    )
    parser.add_argument(
        "-rrv",
        "--rr-variance",
        type=int,
        help="The variance of rr intervals in ms",
        metavar="RR Variance",
        dest="rrv_ms",
        required=False,
        default=100,
    )
    parser.add_argument(
        "-pause",
        "--pause-duration",
        type=int,
        help="The minimum duration of a pause in milliseconds",
        metavar="Minimum Pause Duration",
        dest="pause_dur_ms",
        required=False,
        default=1500,
    )
    parser.add_argument(
        "-pmt",
        "--print-missed-tachy",
        action="store_true",
        help="If present prints missed tachy episodes to a missed episodes file",
        dest="print_tachy",
        required=False,
        default=False,
    )
    parser.add_argument(
        "-pmb",
        "--print-missed-brady",
        action="store_true",
        help="If present prints missed brady episodes to a missed episodes file",
        dest="print_brady",
        required=False,
        default=False,
    )
    parser.add_argument(
        "-pma",
        "--print-missed-afib",
        action="store_true",
        help="If present prints missed afib episodes to a missed episodes file",
        dest="print_afib",
        required=False,
        default=False,
    )
    parser.add_argument(
        "-pmn",
        "--print-missed-normal",
        action="store_true",
        help="If present prints missed normal episodes to a missed episodes file",
        dest="print_normal",
        required=False,
        default=False,
    )
    parser.add_argument(
        "-pmp",
        "--print-missed-pause",
        action="store_true",
        help="If present prints missed pause occurrences to a missed episodes file",
        dest="print_pause",
        required=False,
        default=False,
    )
    parser.add_argument(
        "--skip-noise",
        type=int, 
        help="If value greater than 0, regions marked as unreadable will be removed until [skip_noise ms] after the clean annotation",
        metavar="Skip Noise",
        dest="skip_noise",
        required=False,
        default=0,
    )

    args = parser.parse_args()
    if "Rhythm_Errors_" == args.errors_filename:
        # This inserts the algorithm annotator between the "Rhythm_Errors_" and ".txt" parts
        args.errors_filename = args.alg_ann.join([args.errors_filename, ".txt"])

    corrupt_records_filename = args.alg_ann.join(["Corrupt_Records_", ".txt"])
    missed_episodes_filename = args.alg_ann.join(["Missed_Episodes_", ".txt"])

    print_missed_episodes = {}
    print_missed_episodes["t"] = args.print_tachy
    print_missed_episodes["b"] = args.print_brady
    print_missed_episodes["a"] = args.print_afib
    print_missed_episodes["n"] = args.print_normal
    print_missed_episodes["p"] = args.print_pause

    bypass_beat_check = False

    cwd = os.getcwd()

    database = os.path.basename(os.path.normpath(cwd))

    outfile_name = "{}_stats_{}.txt".format(database, args.alg_ann)

    if os.path.isfile(outfile_name):
        os.remove(outfile_name)

    if database in ["validation_database"]:
        bypass_beat_check = True

    # Don't calculate beat by beat stats for the wfdb, since we don't have those available.
    if not bypass_beat_check:
        print("Comparing beat level annotations ... ", file=sys.stderr)
        sys.stderr.flush()

        compare_beat_annotations.compare_beat_annotations(
        args.alg_ann,
        args.ref_ann,
        outfile_name,
        args.record_filename,
        args.errors_filename,
        args.skip_noise, 
        )

    with open(outfile_name.replace(" ", "_"), mode="a") as outfile:
        # Add a blank line to the output file
        print("", file=outfile)

        if os.path.isfile("Rhythm_Annotations_{}.txt".format(args.ref_ann)):
            os.remove("Rhythm_Annotations_{}.txt".format(args.ref_ann))
        print(
            "Calculating rhythm level annotations for reference data ... ",
            file=sys.stderr,
            end="",
        )
        sys.stderr.flush()
        gen_rhythm_annotations.gen_rhythm_annotations(
            args.ref_ann,
            "Rhythm_Annotations_{}.txt".format(args.ref_ann),
            args.record_filename,
            args.min_episode_onset_duration_ms,
            args.min_episode_exit_duration_ms,
            args.gen_af,
            args.tachy_thresh,
            args.brady_thresh,
            args.rrv_ms,
            args.pause_dur_ms,
            args.skip_noise,
        )
        print("\ndone", file=sys.stderr)

        print("Comparing rhythm level annotations ... ", file=sys.stderr, end="")
        sys.stderr.flush()

        # This function prints its results, so trick it into writing to a
        # file instead.
        sys.stdout = outfile
        stats = compare_rhythm_annotations.compare_rhythm_annotations(
            compare_rhythm_annotations.FileFormat.TZ,
            args.ref_ann,
            args.alg_ann,
            "Rhythm_Annotations_{}.txt".format(args.ref_ann),
            "Rhythm_Annotations_{}.txt".format(args.alg_ann),
            args.record_filename,
            print_missed_episodes,
            corrupt_records_filename,
            missed_episodes_filename,
        )
        compare_rhythm_annotations.print_stats(stats[0], stats[1])

        # Reset stdout.
        sys.stdout = sys.__stdout__

        print("\ndone", file=sys.stderr)
        print("Results are in {}".format(outfile_name.replace(" ", "_")))


if __name__ == "__main__":
    main()
