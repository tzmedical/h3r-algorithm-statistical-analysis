#!/usr/bin/env python3
#
# Author: Dieter Mueller
# This program compares and calculates statistics for beats using bxb.
# Additionally, this program will check for duplicate or out of order
# beats before comparing with the reference.
#
# This program also requires that the wfdb library be installed:
# http://www.physionet.org/physiotools/wfdb.shtml

import sys
import subprocess
import argparse
import platform
import os
from h3rAlgorithmStats import skip_noise_annotations

# The extension that we want the temporary record files to be saved as when the 
# skip_noise flag is set(Ex: h3r_01003.tmp)
TEMPORARY_FILE_EXTENSION = "tmp"

def process_record(record, extension):
    num_dup_beats = 0
    num_ooo_beats = 0

    # Make sure the extension has a "."
    extension = extension if (extension).startswith(".") else "." + extension

    # Use the wfdb utility to read the file
    stream = subprocess.Popen(
        ["ann2rr", "-r", record, "-a", extension[1:]],
        stdout=subprocess.PIPE,
    ).stdout

    # Check for duplicate and out of order beats
    for line in stream:
        if (0 > int(line)):
            num_ooo_beats += 1
        elif (0 == int(line)):
            num_dup_beats += 1

    return num_dup_beats, num_ooo_beats

def compare_beat_annotations(
    algorithm_extension,
    reference_extension,
    outfile_name,
    records_filename,
    errors_filename,
    skip_noise,
):
    # We need to walk through the annotation files files, which contain all the
    # beat data for an extension. We need to check for bad beat files first,
    # then compare the beats using bxb if the file is good.
    corrupted_records = 0
    try:
        with open(records_filename, mode="r") as records_file:
            for record in records_file:
                if record.startswith("#"):
                    continue
                record = record.strip()
                print("Processing record {} - ".format(record), file=sys.stderr, end="")
                num_dup, num_ooo = process_record(record, algorithm_extension)
                print("done; {} duplicates, {} out of order".format(num_dup,num_ooo), file=sys.stderr)
                if ((num_dup > 0) or (num_ooo > 0)):
                    corrupted_records += 1
                # if the skip noise flag is set, call the skip noise function. Skip noise must be nonzero to call the function.  
                if skip_noise: 
                    skip_noise_annotations.skip_noise_annotation(reference_extension, TEMPORARY_FILE_EXTENSION, record, skip_noise)

    except FileNotFoundError as err:
        print("{}: File could not be found.".format(err.filename), file=sys.stderr)
        sys.exit(-1)

    if corrupted_records > 0:
        print("{} record(s) with corrupted beat data, please fix".format(corrupted_records), file=sys.stderr)
        exit(corrupted_records)
    
    # Actually compare the beats with the reference now
    cmd = '"' + os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "stat_analysis"
    )
    if platform.system() != "Linux":
        cmd += ".exe"
    cmd += '"'

    # If the skip noise flag is active, the reference_extension will be the
    # new reference file 
    if skip_noise:
        reference_extension = TEMPORARY_FILE_EXTENSION

    # otherwise it is whatever is passed into this function
    res = subprocess.Popen(
        " ".join([
            cmd,
            outfile_name.replace(" ", "_"),
            algorithm_extension,
            reference_extension,
            records_filename,
            errors_filename,
        ]),
        shell=True,
        stderr=subprocess.PIPE,
    )
    res.wait()
    if 0 != res.returncode:
        print(
            "\nexit code {}: {}".format(
                res.returncode, res.stderr.read().decode("ascii").strip()
            )
        )
        exit(res.returncode)
    print("\ndone", file=sys.stderr)
    return None


def main():
    """
    Run the program
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-alg",
        "--alg-annotator",
        type=str,
        help="The algorithm annotator extension",
        metavar="Algorithm Annotator Extension",
        dest="alg_ann",
        required=True,
    )
    parser.add_argument(
        "-ref",
        "--ref-annotator",
        type=str,
        help="The reference annotator extension (default atr)",
        metavar="Reference Annotator Extension",
        dest="ref_ann",
        required=False,
        default="atr",
    )
    parser.add_argument(
        "-rec",
        "--records",
        type=str,
        help="The records filename (usually RECORDS.txt)",
        metavar="Records Filename",
        dest="record_filename",
        required=False,
        default="RECORDS.txt",
    )
    parser.add_argument(
        "-err",
        "--error-output",
        type=str,
        help="The errors filename containing a list of records with beat-by-beat errors (default Rhythm_Errors_[algorithm annotator].txt)",
        metavar="Errors Filename",
        dest="errors_filename",
        required=False,
        default="Rhythm_Errors_",
    )
    parser.add_argument(
        "--skip-noise",
        type=int,
        help="If value greater than 0, regions marked as unreadable will be removed until [skip_noise ms] after the clean annotation",
        metavar="Skip Noise",
        dest="skip_noise",
        required=False,
        default=0,
    )

    args = parser.parse_args()

    if "Rhythm_Errors_" == args.errors_filename:
        # This inserts the algorithm annotator between the "Rhythm_Errors_" and ".txt" parts
        args.errors_filename = args.alg_ann.join([args.errors_filename, ".txt"])

    # Make sure our extensions do not begin with a "."
    if args.alg_ann[0].startswith("."):
        args.alg_ann[0] = args.alg_ann[0][1:]
    if args.ref_ann[0].startswith("."):
        args.ref_ann[0] = args.ref_ann[0][1:]

    database = os.path.basename(os.path.normpath(cwd))

    outfile_name = "{}_stats_{}.txt".format(database, args.alg_ann)

    compare_beat_annotations(
        args.alg_ann[0],
        args.ref_ann[0],
        outfile_name,
        args.record_filename[0],
        args.errors_filename[0], 
    )
    
if __name__ == "__main__":
    main()
