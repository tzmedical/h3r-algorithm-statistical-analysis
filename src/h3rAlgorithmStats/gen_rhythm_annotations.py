#!/usr/bin/env python3
#
# Author: Jeremy Sigrist & Dieter Mueller
# This program processes wfdb annotation files and generates a separate rhythm
# file which contains brady, tachy, a-fib, and pause annotations.

import subprocess
import sys
from .annotation_info import AnnotationLabel
from .annotation_info import AnnotationName

# Print debug information to the output file?
DEBUG_INFO = False

# Number of beats to average when calculating average
NUM_RECENT_HEARTRATE = 8

# Default average heart rate
DEFAULT_AVERAGE_HEART_RATE = 80 

# Number of seconds in a minute
SEC_PER_MINUTE = 60

# Millisecond offset to denote pause
PAUSE_DT = 150

# A BAR symbol in the transitions represents a "QRS-like noise spike"
BAR = "|"
# A TILDE symbol in the transitions represents a change in signal quality
NOISE = "~"
# A PLUS symbol in the transitions represents a rhythm change 
RHYTHM_CHANGE = "+"
# A letter U in the transitions represents an unreadable signal
UNANALYZABLE = "u"
# A letter C in the transitions represents a clean signal 
CLEAN = "c"

wfdb_ecg_codes = {
    "(AFIB": AnnotationLabel.AF,
    "(AFL": AnnotationLabel.AFL,
    "(N": AnnotationLabel.NSR,
}


class RhythmAnnotation:
    """
    Holds information about annotations to print.
    """

    def __init__(self, record, start_time, label):
        self.record = record
        self.start_time = start_time
        self.label = label


def gen_rhythm_annotations(
    extension,
    rhythm_filename,
    record_filename="RECORDS.txt",
    min_episode_onset_duration_ms=6000,
    min_episode_exit_duration_ms=6000,
    gen_af=True,
    tachy_thresh=100,
    brady_thresh=60,
    rrv_ms=100,
    pause_dur_ms=1500,
    skip_noise=0,
):
    rr_ms = ((SEC_PER_MINUTE * 1000) / tachy_thresh) + rrv_ms
    tachy_exit = (SEC_PER_MINUTE * 1000) / rr_ms
    rr_ms = ((SEC_PER_MINUTE * 1000) / brady_thresh) - rrv_ms
    brady_exit = (SEC_PER_MINUTE * 1000) / rr_ms
    print(
        "\nTACHY: Onset: {} \t Offset: {}".format(tachy_thresh, tachy_exit),
        file=sys.stderr,
        end="",
    )
    print(
        "\nBRADY: Onset: {} \t Offset: {}".format(brady_thresh, brady_exit),
        file=sys.stderr,
        end="",
    )
    with open(rhythm_filename, mode="w") as output_file, open(
        record_filename, mode="r"
    ) as records_file:
        output_file.write("Record\tType\tStart\n")
        for record in records_file:
            if record.startswith("#"):
                continue
            record = record.strip()
            # print("Processing record {}".format(record))
            # get sample frequency using the wfdb library function
            stream = subprocess.Popen(
                ["sampfreq", record], stdout=subprocess.PIPE
            ).stdout
            process_file(
                record,
                extension,
                output_file,
                int(stream.read()),
                min_episode_onset_duration_ms,
                min_episode_exit_duration_ms,
                gen_af,
                tachy_thresh,
                tachy_exit,
                brady_thresh,
                brady_exit,
                pause_dur_ms,
                skip_noise,
            )


def process_file(
    record,
    extension,
    output_file,
    sample_frequency,
    min_episode_onset_duration_ms,
    min_episode_exit_duration_ms,
    gen_af,
    tachy_threshold,
    tachy_exit,
    brady_threshold,
    brady_exit,
    pause_dur_ms,
    skip_noise,
):
    # This is a list of the previous few gaps between heartbeats.
    avg_heartrate_list = [sample_frequency] * NUM_RECENT_HEARTRATE
    avg_heartrate = DEFAULT_AVERAGE_HEART_RATE
    sample_count = 0

    # Make sure the extension has a "."
    extension = extension if extension.startswith(".") else "." + extension
    input_filename = record + extension

    # Use the wfdb utility to read the file
    stream = subprocess.Popen(
        ["ann2rr", "-r", record, "-a", extension[1:]],
        stdout=subprocess.PIPE,
    ).stdout
    gaps = [int(line) for line in stream]

    stream = subprocess.Popen(
        ["rdann", "-r", record, "-a", extension[1:], "-e", "-p", RHYTHM_CHANGE, NOISE],
        stdout=subprocess.PIPE,
    ).stdout

    # This is a sample:type:label (ex. [1000, '|', ''] or [1200, '+', '(N']) triple for the transitions of the record
    transitions = (
        [int(line.split()[1]), line.split()[2].decode(), line.split()[-1].decode()]
        for line in stream
    )
    transition_done = False

    try:
        transition = next(transitions)
    except StopIteration:
        transition_done = True

    # duration (ms) * frequency (samples/s) / 1000 (ms/s) = duration (samples)
    min_episode_onset_sample = min_episode_onset_duration_ms * sample_frequency / 1000
    min_episode_exit_sample = min_episode_exit_duration_ms * sample_frequency / 1000

    output = Rhythm_Output(
        min_episode_onset_sample, min_episode_exit_sample, output_file
    )

    if DEBUG_INFO:
        print(
            "Episode Onset Threshold: {}(ms) == {} samples".format(
                min_episode_onset_duration_ms, min_episode_onset_sample
            ),
            file=sys.stderr,
        )
        print(
            "Episode Exit Threshold: {}(ms) == {} samples".format(
                min_episode_exit_duration_ms, min_episode_exit_sample
            ),
            file=sys.stderr,
        )

    # Start every file with an unknown state (in case the first transition is
    # beyond the first beat and we need to handle a state check)

    current_state = AnnotationLabel.UNKNOWN
    current_episode = RhythmAnnotation(record, 0, AnnotationLabel.UNKNOWN)
    episode_confirmed = False
    previous_state = None
    next_state = None
    skip_gap = False
    averaged_gaps = 0
    last_confirmed = None
    analysis_active = True
    skip_noise_samples = 0

    # print("Record {}".format(record),file = sys.stderr)
    # Iterate through the gaps in the file, using a state machine to keep track
    # of different rhythm states.
    i = -1
    while i < (len(gaps) - 1):
        # print("Gap {}: {}".format(i, gaps[i]), file = sys.stderr)
        next_state = current_state

        # If the next beat is beyond the next transition, deal with the transition first
        if (
            (not transition_done)
            and (sample_count < transition[0])
            and (sample_count + gaps[i + 1] > transition[0])
        ):
            #print("Transition {}".format(transition[0]), file=sys.stderr)

            # Check the episode length again with the position of the transition
            if not episode_confirmed:
                episode_confirmed = output.confirm(current_episode, transition[0])
                # If we've been in this state long enough, latch it
                if episode_confirmed:
                    last_confirmed = current_episode.label
            try:
                if transition[1] == BAR:
                    # Ditch the gap, so that bars (pseudo-QRS's) don't get counted towards heart rate.
                    skip_gap = True
                    # print("Bar found at {}. Skipping Gap {} with length {}".format(transition[0], i, gaps[i]), file=sys.stderr)
                elif transition[1] == NOISE:
                    if skip_noise:
                        if transition[2] == UNANALYZABLE:
                            # Entering noisy region
                            analysis_active = False
                            current_episode = RhythmAnnotation(
                                record, transition[0], AnnotationLabel.UNANALYZABLE
                            )
                            next_state = AnnotationLabel.UNANALYZABLE
                            episode_confirmed = output.confirm(current_episode, transition[0])
                            if episode_confirmed:
                                last_confirmed = current_episode.label
                        elif transition[2] == CLEAN:
                            # Reset heart rate calculations
                            avg_heartrate_list = [
                                sample_frequency
                            ] * NUM_RECENT_HEARTRATE
                            avg_heartrate = DEFAULT_AVERAGE_HEART_RATE
                            averaged_gaps = 0
                            # Get the samples to wait until analysis can be active again after the C flag
                            # is set
                            skip_noise_samples = (sample_frequency * skip_noise) / 1000
                            episode_confirmed = False

                elif transition[1] == RHYTHM_CHANGE:
                    if gen_af:
                        if (wfdb_ecg_codes[transition[2]] == AnnotationLabel.AF) or (
                            wfdb_ecg_codes[transition[2]] == AnnotationLabel.AFL
                        ):
                            current_episode = RhythmAnnotation(
                                record, transition[0], AnnotationLabel.AF
                            )
                            next_state = AnnotationLabel.AF
                        elif wfdb_ecg_codes[transition[2]] == AnnotationLabel.NSR:
                            if AnnotationLabel.AF == current_state:
                                current_episode = RhythmAnnotation(
                                    record, transition[0], AnnotationLabel.NSR
                                )
                                next_state = AnnotationLabel.NSR
            # KeyError means we don't know this transition, so ignore it
            except KeyError:
                pass
                # current_episode = RhythmAnnotation(record, transition[0], AnnotationLabel.UNKNOWN)
                # next_state = AnnotationLabel.UNKNOWN

            # Regardless, move forward if there is another transition
            try:
                transition = next(transitions)
            except StopIteration:
                transition_done = True
        # Else there is another beat to deal with
        else:
            # Move the gap marker forward as we deal with the next beat
            i = i + 1
            # Keep the sample count correct regardless of if we ignore the effects of the gap
            sample_count += gaps[i]

            # Check noise samples to see if we hit the end of the noisy region 
            # We want to wait the amount of samples after the C flag happens            
            if (skip_noise_samples > gaps[i]):
                skip_noise_samples -= gaps[i]
            elif (skip_noise_samples):
                # if the samples exceeds the noisy region then complete the analysis
                skip_noise_samples = 0
                analysis_active = True

            if analysis_active:
                # Check if the episode is long enough after moving to the new position
                if not episode_confirmed:
                    episode_confirmed = output.confirm(current_episode, sample_count)
                    # If we've been in this state long enough, latch it
                    if episode_confirmed:
                        last_confirmed = current_episode.label
                # One-off, just ignore the rest of the loop
                if skip_gap:
                    # Clear the flag
                    skip_gap = False
                    # Keep the HR as accurate as possible
                    avg_heartrate_list[i % NUM_RECENT_HEARTRATE] = (
                        sample_frequency * SEC_PER_MINUTE
                    ) / avg_heartrate
                    continue

                # guard from entering pause gaps into the list
                # if gaps[i] > (sample_frequency * pause_dur_ms) / 1000:
                #    #print("\nIgnoring Paused gap. Using: {}".format((sample_frequency * SEC_PER_MINUTE) / avg_heartrate), file = sys.stderr)
                #    avg_heartrate_list[i % NUM_RECENT_HEARTRATE] = (sample_frequency * SEC_PER_MINUTE) / avg_heartrate
                # else:
                avg_heartrate_list[i % NUM_RECENT_HEARTRATE] = gaps[i]

                # Use integer division here to best emulate the C code.
                # avg_heartrate = sample_frequency * SEC_PER_MINUTE * NUM_RECENT_HEARTRATE // sum(avg_heartrate_list)
                # Not using interger division
                avg_heartrate = (
                    sample_frequency
                    * SEC_PER_MINUTE
                    * NUM_RECENT_HEARTRATE
                    / sum(avg_heartrate_list)
                )

                # print("8 beat avg: {:.2f} \tstate: {} \tsample: {}".format(avg_heartrate, current_state, sample_count))

                if DEBUG_INFO:
                    # Print debugging information to output file.
                    print(
                        "record = {}, gaps[i] = {}, sample_count_file = {}, heartrate = {:.2f}".format(
                            record, gaps[i], sample_count, avg_heartrate
                        ),
                        file=output_file,
                    )
                # Check for pause
                if (0 != averaged_gaps) and (current_state != AnnotationLabel.UNANALYZABLE):
                    if gaps[i] > (sample_frequency * pause_dur_ms) / 1000:
                        output.write_annotation(
                            RhythmAnnotation(
                                record, sample_count - gaps[i], AnnotationLabel.PAUSE
                            )
                        )
                    # Check if we should make allowances for a pause in the algorithm
                    elif (
                        gaps[i] > (sample_frequency * (pause_dur_ms - PAUSE_DT)) / 1000
                    ):
                        output.write_annotation(
                            RhythmAnnotation(
                                record,
                                sample_count - gaps[i],
                                AnnotationLabel.PSEUDO_PAUSE,
                            )
                        )

                # Latch to remove Brady/Tachy checking until the avg_heartrate list is full
                if averaged_gaps < NUM_RECENT_HEARTRATE:
                    # Move the number of averaged gaps forward
                    averaged_gaps = averaged_gaps + 1
                    continue

                # Depending on what state we're in, create a new annotation if we change
                if (current_state == AnnotationLabel.UNKNOWN) or (current_state == AnnotationLabel.UNANALYZABLE):
                    if (current_state == AnnotationLabel.UNANALYZABLE) and (
                        previous_state == AnnotationLabel.AF
                    ):
                        current_episode = RhythmAnnotation(
                            record, transition[0], AnnotationLabel.AF
                        )
                        next_state = AnnotationLabel.AF   
                    elif avg_heartrate > tachy_threshold:
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.TACHY
                        )
                        next_state = AnnotationLabel.TACHY
                    elif avg_heartrate < brady_threshold:
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.BRADY
                        )
                        next_state = AnnotationLabel.BRADY
                    else:
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.NSR
                        )
                        next_state = AnnotationLabel.NSR
                    
                elif current_state == AnnotationLabel.NSR:
                    if (avg_heartrate > tachy_exit) and (
                        last_confirmed == AnnotationLabel.TACHY
                    ):
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.TACHY
                        )
                        next_state = AnnotationLabel.PSEUDO_TACHY
                    elif avg_heartrate > tachy_threshold:
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.TACHY
                        )
                        next_state = AnnotationLabel.TACHY

                    elif (avg_heartrate < brady_exit) and (
                        last_confirmed == AnnotationLabel.BRADY
                    ):
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.BRADY
                        )
                        next_state = AnnotationLabel.PSEUDO_BRADY
                    elif avg_heartrate < brady_threshold:
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.BRADY
                        )
                        next_state = AnnotationLabel.BRADY

                elif current_state == AnnotationLabel.TACHY:
                    if avg_heartrate < tachy_exit:
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.NSR
                        )
                        next_state = AnnotationLabel.NSR
                    elif (avg_heartrate < tachy_threshold) and (not episode_confirmed):
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.NSR
                        )
                        next_state = AnnotationLabel.NSR

                elif current_state == AnnotationLabel.PSEUDO_TACHY:
                    if avg_heartrate < tachy_exit:
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.NSR
                        )
                        next_state = AnnotationLabel.NSR

                elif current_state == AnnotationLabel.BRADY:
                    if avg_heartrate > brady_exit:
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.NSR
                        )
                        next_state = AnnotationLabel.NSR
                    elif (avg_heartrate > brady_threshold) and (not episode_confirmed):
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.NSR
                        )
                        next_state = AnnotationLabel.NSR

                elif current_state == AnnotationLabel.PSEUDO_BRADY:
                    if avg_heartrate > brady_exit:
                        current_episode = RhythmAnnotation(
                            record, sample_count, AnnotationLabel.NSR
                        )
                        next_state = AnnotationLabel.NSR

        if next_state != current_state:
            previous_state = current_state
            current_state = next_state
            episode_confirmed = False

    # Once the beat by beat information is done, make sure all the transitions are exhausted
    while not transition_done:
        next_state = current_state
        # Check the episode length again with the position of the transition
        if not episode_confirmed:
            episode_confirmed = output.confirm(current_episode, transition[0])
            # If we've been in this state long enough, latch it
            if episode_confirmed:
                last_confirmed = current_episode.label
        try:
            if transition[1] == BAR:
                # Just ignore bars completely at this point
                pass
            elif gen_af:
                # Assume the transition is a '+', in which case we need to look at the extended description
                if (wfdb_ecg_codes[transition[2]] == AnnotationLabel.AF) or (
                    wfdb_ecg_codes[transition[2]] == AnnotationLabel.AFL
                ):
                    current_episode = RhythmAnnotation(
                        record, transition[0], AnnotationLabel.AF
                    )
                    next_state = AnnotationLabel.AF
                elif wfdb_ecg_codes[transition[2]] == AnnotationLabel.NSR:
                    if AnnotationLabel.AF == current_state:
                        current_episode = RhythmAnnotation(
                            record, transition[0], AnnotationLabel.NSR
                        )
                        next_state = AnnotationLabel.NSR
        # KeyError means we don't know this transition
        except KeyError:
            pass
            # current_episode = RhythmAnnotation(record, transition[0], AnnotationLabel.UNKNOWN)
            # next_state = AnnotationLabel.UNKNOWN

        # Regardless, move forward if there is another transition
        try:
            transition = next(transitions)
        except StopIteration:
            transition_done = True

        if next_state != current_state:
            previous_state = current_state
            current_state = next_state
            episode_confirmed = False


class Rhythm_Output:
    def __init__(self, min_episode_onset_sample, min_episode_exit_sample, output_file):
        self._previous_annotation = None
        self._min_episode_onset_sample = min_episode_onset_sample
        self._min_episode_exit_sample = min_episode_exit_sample
        self._output_file = output_file

    # This checks if an episode is long enough , and if it is it writes it out
    def confirm(self, annotation, sample_count):
        # If there is no episode, just leave
        if (None == annotation) or (AnnotationLabel.UNKNOWN == annotation.label):
            return False

        # Assume we'll write the episode if it's confirmed
        write_ep = True
        confirmed = False
        
        # If there was a previous episode and it's the same, we don't want to write this one
        if (self._previous_annotation != None) and (
            self._previous_annotation.label == annotation.label
        ):
            write_ep = False
        
        # If this episode is a U, confirm it
        if (AnnotationLabel.UNANALYZABLE == annotation.label):
            confirmed = True

        # If the previous was not Brady or Tachy, check onset duration
        elif (self._previous_annotation == None) or (
            self._previous_annotation.label == AnnotationLabel.NSR) or (
            self._previous_annotation.label == AnnotationLabel.UNANALYZABLE
        ):
            confirmed = (sample_count - annotation.start_time >= self._min_episode_onset_sample)
        # Else check the offset duration
        else:
            confirmed = (sample_count - annotation.start_time >= self._min_episode_exit_sample)
            
        if (confirmed and write_ep):
            self._previous_annotation = self.write_annotation(annotation)
        return confirmed

    def write_annotation(self, annotation):
        self._output_file.write(
            "{}\t{}\t{}\n".format(
                annotation.record, annotation.label, annotation.start_time
            )
        )
        return annotation


if __name__ == "__main__":
    if len(sys.argv) != 10:
        print(
            "Usage: gen_rhythm_annotations.py [extension] [output file] [record file] [mimimun episode duration] [gen_af] [tachy_thresh] [brady_thresh] [rr_variance] [min_pause_dur]",
            file=sys.stderr,
        )
        exit(-1)

    gen_rhythm_annotations(
        sys.argv[1],
        sys.argv[2],
        sys.argv[3],
        sys.argv[4],
        sys.argv[5],
        sys.argv[6],
        sys.argv[7],
        sys.argv[8],
        sys.argv[9],
    )
