#!/usr/bin/env python3
#
# Author: Robert Rodarte (Help from Dieter)
# This program will skip over the noise between the U and C flags
# using a temporary file which can be used as a reference. The temporary
# file removes the noisy data.  
#
# This program also requires that the wfdb library be installed:
# http://www.physionet.org/physiotools/wfdb.shtml

import subprocess

# A TILDE symbol in the transitions represents a change in signal quality
NOISE = "~"
# A letter U in the transitions represents an unreadable signal
UNANALYZABLE = "u"
# A letter C in the transitions represents a clean signal 
CLEAN = "c"
# The temporary file that will be written without the noise and passed into bxb.c as a reference 
TEMPORARY_FILENAME = 'temp.tmp'

def skip_noise_annotation(
    extension, # extension of the file (ex. ".txt")
    out_ext,  # output extension (annotation)
    record,   # record to remove beats from
    skip_noise, # Value of skip noise
):  
    # If skip noise is 0, it shouldn't even reach this point but if it does
    # this is our check. 
    if skip_noise == 0: 
        print("Invalid Arg: Skip noise must be nonzero")
        return
    
    # get sample frequency using the wfdb library function
    stream = subprocess.Popen(
        ["sampfreq", record], stdout=subprocess.PIPE
    ).stdout

    sample_frequency = int(stream.read()) # Value of sample frequency
        
    # mode W has a windows default newline of \r\n
    with open("temp.tmp", mode="w", newline='\n') as output_file:
        # Use the wfdb utility to read the file 
        # Read in file
        stream = subprocess.Popen(
            ["rdann", "-r", record, "-a", extension],
            stdout=subprocess.PIPE,
        ).stdout

        # Initialize 
        zeroData = False
        sample_count = 0
  
        # Convert the skip_noise time from ms to samples
        skip_noise_samples = (sample_frequency * skip_noise) / 1000

        # Loop through file, removing any beats between u and c marks
        # This may be expanded to remove a beat or two after the c mark
        for line in stream:
            #print(line.decode().strip())
            data = [int(line.split()[2]), line.split()[3].decode(), line.split()[-1].decode()]
            if data[1] == NOISE:
                if data[2] == UNANALYZABLE: # Entering noisy region
                    # remove data 
                    zeroData = True
                elif data[2] == CLEAN: # Leaving noisy region 
                    # Grab the sample count of the C flag 
                    sample_count = data[0]
                # Write the U and C lines before leaving 
                output_file.write(line.decode())

            # If we are removing data, keep removing data until we hit the desired amount
            # of samples after the C flag 
            if (sample_count) and ((sample_count + skip_noise_samples) < data[0]):
                # stop removing data
                zeroData = False
                sample_count = 0

            # if the data is not being removed then write the data into the output file.
            if not zeroData:
                try: 
                    output_file.write(line.decode())
                except:
                    # The "Aux" character can not be encoded so we throw it out
                    pass

    # Write file out, make sure to use a different annotation, but same record name
    # Write the new record file with the contents of the output_file 
    # The output_file should have everything except the noisy regions. 
    with open(TEMPORARY_FILENAME, 'r') as file:
        subprocess.Popen(["wrann", "-r", record, "-a", out_ext], stdin=file)
    