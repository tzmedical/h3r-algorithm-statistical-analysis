#!/bin/bash

set -e

# Detect Linux and Windows
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac


echo
echo "Checking Python version..."
pythonVersion=3.8.10
# Warn the user if they are using a different python version
if ! python3 --version 2>/dev/null | grep -q "$pythonVersion"; then
  echo
  echo "WARNING: python3 version ($(python --version)) doesn't match target version (Python $pythonVersion)"
  if [[ "CygwinMinGw" == *"$machine"* ]]; then
    echo "Try running the following commands:"
    echo "  choco install python --version=$pythonVersion -y --params \"/InstallDir:C:\\\\Python38\""
    echo '  ln -s "$(which python)" "$(which python)"3'
    echo
  fi
fi

echo
echo "Installing python build tools..."
pip install -q build virtualenv setuptools

if [[ "CygwinMinGw" == *"$machine"* ]]; then
  echo
  echo "Fixing WFDB library linking..."
  ln -s /usr/local/lib/libwfdb.dll.a /usr/local/lib/wfdb.lib 2> /dev/null || true
fi

echo
echo "Compiling C code into command-line executable..."
make

echo
echo "Building Python scripts into an installable module..."
python3 -m build

echo
echo "Installing Python module for command-line use..."
python -m pip install --user .

if [[ "CygwinMinGw" == *"$machine"* ]]; then
echo
echo 'You may need to add something like the following line to you "~/.bash_profile" file:'
echo '  export PATH="~/AppData/Roaming/Python/Python38/Scripts/":$PATH'
fi

echo
echo "Done! Verify success by running:"
echo "  h3rAlgorithmStats --version"
